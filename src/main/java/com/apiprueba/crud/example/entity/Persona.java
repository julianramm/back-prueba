package com.apiprueba.crud.example.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@SequenceGenerator(name="seq", initialValue=11, allocationSize=1)
public class Persona {

    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="seq")
    private int id_persona ;
    private String nombre;
    private String apellido;
    private int edad;
    private String sexo;
    private String correo;

    public int getId_persona() {
        return id_persona;
    }

    public void setId_persona(int id) {
        this.id_persona = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String email) {
        this.correo = email;
    }

}
