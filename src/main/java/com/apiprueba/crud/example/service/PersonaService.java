package com.apiprueba.crud.example.service;

import com.apiprueba.crud.example.entity.Persona;
import com.apiprueba.crud.example.repository.PersonaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonaService {

    @Autowired
    private PersonaRepository repository;

    public Persona crearPersona(Persona persona){
        return repository.save(persona);
    }
    public List<Persona> crearPersonas(List<Persona> personas){
        return repository.saveAll(personas);
    }
    public void eliminarPersona(int id){
        repository.deleteById(id);
    }
    public void eliminarPersonas(){
        repository.deleteAll();
    }
    public Persona actualizarPersona(Persona persona){
        Persona person = repository.findById(persona.getId_persona()).orElse(null);
        person.setNombre(persona.getNombre());
        person.setApellido(persona.getApellido());
        person.setEdad(persona.getEdad());
        person.setSexo(persona.getSexo());
        person.setCorreo(persona.getCorreo());
        return repository.save(person);
    }
    public List<Persona> obtenerPersonas(){
        return repository.findAll();
    }
    public Persona obtenerPersona(int id){
        return repository.findById(id).orElse(null);
    }

}
