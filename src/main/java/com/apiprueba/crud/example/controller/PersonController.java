package com.apiprueba.crud.example.controller;

import com.apiprueba.crud.example.entity.Persona;
import com.apiprueba.crud.example.service.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {

    @Autowired
    private PersonaService service;

    @CrossOrigin
    @PostMapping("/addPersona")
    public Persona addPersona(@RequestBody Persona persona ){
        return service.crearPersona(persona);
    }
    @CrossOrigin
    @PostMapping("/addPersonas")
    public List<Persona> addPersonas(@RequestBody List<Persona> personas ){
        return service.crearPersonas(personas);
    }
    @CrossOrigin
    @GetMapping("/personas")
    public List<Persona> getPersonas(){
        return service.obtenerPersonas();
    }
    @CrossOrigin
    @GetMapping("/persona/{id}")
    public Persona getPersonaPorId(@PathVariable int id){
        return service.obtenerPersona(id);
    }
    @CrossOrigin
    @DeleteMapping("/delete/{id}")
    public void deletePersonaPorId(@PathVariable int id ){
        service.eliminarPersona(id);
    }
    @CrossOrigin
    @DeleteMapping("/delete")
    public void deletePersonas(){
        service.eliminarPersonas();
    }
    @CrossOrigin
    @PutMapping("/update")
    public Persona updatePersona(@RequestBody Persona persona ){
        return service.actualizarPersona(persona);
    }

}
