package com.apiprueba.crud.example.repository;

import com.apiprueba.crud.example.entity.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonaRepository extends JpaRepository<Persona, Integer> {

}
